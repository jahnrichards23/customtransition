//
//  MainViewController.m
//  CustomTransitionNC
//
//  Created by Richard John Alamer on 05/10/2016.
//  Copyright © 2016 Richard John Alamer. All rights reserved.
//

#import "MainViewController.h"
#import "SecondViewController.h"
#import "ModalAnimator.h"

@interface MainViewController () <UINavigationControllerDelegate>

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.delegate = self;
}

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController*)fromVC
                                                 toViewController:(UIViewController*)toVC
{
    NSLog(@"present");
    
    ModalAnimator *animator = [ModalAnimator new];
    animator.presenting = (operation == UINavigationControllerOperationPush);
    return animator;
}



-(IBAction)goToNextView:(id)sender {
    
    SecondViewController *secondView = [[SecondViewController alloc]initWithNibName:@"SecondViewController" bundle:nil];
    
    [self.navigationController pushViewController:secondView animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
