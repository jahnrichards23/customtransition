//
//  ModalAnimator.m
//  CustomTransitionNC
//
//  Created by Richard John Alamer on 05/10/2016.
//  Copyright © 2016 Richard John Alamer. All rights reserved.
//

#import "ModalAnimator.h"

@implementation ModalAnimator


- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 0.5;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    
    UIViewController* toViewController   = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    UIView* container = transitionContext.containerView;
    
    if (self.presenting) {
    
    [container.superview sendSubviewToBack:container];
    
    fromViewController.view.frame = container.bounds;
    toViewController.view.frame = container.bounds;

    
    [[transitionContext containerView] addSubview:toViewController.view];
    [fromViewController beginAppearanceTransition:YES animated:YES];
    toViewController.view.alpha = 0.0;
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        toViewController.view.alpha = 1.0;
        fromViewController.view.frame = [self presentingControllerFrameWithContext:transitionContext];

        
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
    }];
    }
    else {
        
        UIViewController* toViewController   = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
        UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
        
        UIView* container = transitionContext.containerView;
        
        fromViewController.view.frame = container.bounds;
        toViewController.view.frame = container.bounds;
        
        
        [[transitionContext containerView] addSubview:toViewController.view];
        
        toViewController.view.alpha = 0.0;
        
        
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
            toViewController.view.alpha = 1.0;
            fromViewController.view.frame = [self dismissingControllerFrameWithContext:transitionContext];
            [fromViewController beginAppearanceTransition:YES animated:YES];
            
            
        } completion:^(BOOL finished) {
            [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
        }];


    }
    
}


- (CGRect)presentingControllerFrameWithContext:(id<UIViewControllerContextTransitioning>)transitionContext {
    CGRect frame = transitionContext.containerView.bounds;
    
    if(floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1)
    {
        return CGRectMake(0, -CGRectGetHeight(frame), CGRectGetWidth(frame), CGRectGetHeight(frame));
    }
    else
    {
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        switch (orientation) {
            case UIInterfaceOrientationLandscapeLeft:
                return CGRectMake(CGRectGetWidth(frame), 0, CGRectGetWidth(frame), CGRectGetHeight(frame));
                break;
            case UIInterfaceOrientationLandscapeRight:
                return CGRectMake(-CGRectGetWidth(frame), 0, CGRectGetWidth(frame), CGRectGetHeight(frame));
                break;
            case UIInterfaceOrientationPortraitUpsideDown:
                return CGRectMake(0, -CGRectGetHeight(frame), CGRectGetWidth(frame), CGRectGetHeight(frame));
                break;
            default:
            case UIInterfaceOrientationPortrait:
                return CGRectMake(0, CGRectGetHeight(frame), CGRectGetWidth(frame), CGRectGetHeight(frame));
                break;
        }
    }
}

- (CGRect)dismissingControllerFrameWithContext:(id<UIViewControllerContextTransitioning>)transitionContext {
    CGRect frame = transitionContext.containerView.bounds;
    
    if(floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1)
    {
        return CGRectMake(0, CGRectGetHeight(frame), CGRectGetWidth(frame), CGRectGetHeight(frame));
    }
    else
    {
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        switch (orientation) {
            case UIInterfaceOrientationLandscapeLeft:
                return CGRectMake(CGRectGetWidth(frame), 0, CGRectGetWidth(frame), CGRectGetHeight(frame));
                break;
            case UIInterfaceOrientationLandscapeRight:
                return CGRectMake(-CGRectGetWidth(frame), 0, CGRectGetWidth(frame), CGRectGetHeight(frame));
                break;
            case UIInterfaceOrientationPortraitUpsideDown:
                return CGRectMake(0, -CGRectGetHeight(frame), CGRectGetWidth(frame), CGRectGetHeight(frame));
                break;
            default:
            case UIInterfaceOrientationPortrait:
                return CGRectMake(0, CGRectGetHeight(frame), CGRectGetWidth(frame), CGRectGetHeight(frame));
                break;
        }
    }
}


#pragma mark - Private
- (UIViewController *)topViewControllerForViewController:(UIViewController *)viewController {
    if ([viewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *nc = (UINavigationController *)viewController;
        return nc.topViewController;
    } else {
        return viewController;
    }
}







@end
