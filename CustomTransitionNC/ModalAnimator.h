//
//  ModalAnimator.h
//  CustomTransitionNC
//
//  Created by Richard John Alamer on 05/10/2016.
//  Copyright © 2016 Richard John Alamer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ModalAnimator : NSObject<UIViewControllerAnimatedTransitioning> {
 
    
    
    BOOL presenting;
}
@property (nonatomic) BOOL presenting;

@end
