//
//  AppDelegate.h
//  CustomTransitionNC
//
//  Created by Richard John Alamer on 05/10/2016.
//  Copyright © 2016 Richard John Alamer. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MainViewController *viewController;

@end

