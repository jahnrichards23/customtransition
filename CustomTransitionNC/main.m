//
//  main.m
//  CustomTransitionNC
//
//  Created by Richard John Alamer on 05/10/2016.
//  Copyright © 2016 Richard John Alamer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
